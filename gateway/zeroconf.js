/**
 * @module sece/srv/gateway/zeroconf
 */

import debug           from 'debug';
import dbus            from 'dbus-native';
import avahi           from 'avahi-dbus';
import e2              from 'eventemitter2';
import {ResourceURI}   from '@sece/lib';
import {service_types} from './main.js';


const sysbus = dbus.systemBus(),
      daemon = new avahi.Daemon(sysbus);

const dbg = debug('sece:gateway:zeroconf');


// Discover the set of advertised service types on the local network:
// _services._dns-sd._udp.local     PTR _http._tcp.local.
// Discover the list of instances that provide a certain service:
// _http._tcp.local.                PTR SomeUniqueName._http._tcp.local.
// SomeUniqueName._http._tcp.local. SRV 0 0 5060 rimmer.local.


function parse_service(srv)
{
  let c = srv.split('.');
  if (c.length != 4) {
    throw new Error('Invalid service name');
  }

  return {
    name: c[0],
    type: `${c[1]}.${c[2]}`,
    domain: c[3]
  };
}


function parse_txt(txt)
{
  if (!txt) return [];
  txt = txt.filter(t => t.type == 'Buffer');
  return txt.map(t => new Buffer(t.data).toString());
}


export function resolve_srv(service)
{
  const {name, type, domain} = parse_service(service);

  dbg(`Resolving service ${service}`);
  return new Promise((res, rej) => {
    daemon.ResolveService(avahi.IF_UNSPEC, avahi.PROTO_INET, name, type, domain, avahi.PROTO_UNSPEC, 0, (
      err, iface, proto, name, type, domain, host, aproto, addr, port, txt, flags) => {
	if (err) {
	  dbg(`Couldn't resolve ${service}`);
	  rej(err);
	} else {
	  dbg(`Resolved to ${addr}:${port}`);
	  res({
	    host: host,
	    addr: addr,
	    port: port,
	    txt: parse_txt(txt)
	  });
	}
      });
  });
}


async function create_browser(type, add, remove)
{
  dbg(`Starting ZeroConf service browser for ${type}`);
  return new Promise((resolve, reject) => {
    daemon.ServiceBrowserNew(avahi.IF_UNSPEC, avahi.PROTO_UNSPEC, type.toString(), 'local', 0, (err, browser) => {
      if (err) {
	reject(err);
      } else {
	browser.on('ItemNew', add);
	browser.on('ItemRemove', remove);
      }
    });
  });
}


/**
 * Convert a ZeroConf service type for a graph node object.
 */
function service2node(uri, s) {
  let t = ['service', 'zeroconf'];
  if (uri.toString().includes('_smob._tcp')) {
    t.push('smob');
  }
  return {
    uri: uri,
    label: s.name,
    type: t
  };
};


export function InstanceBrowser(type)
{
  e2.EventEmitter2.call(this);
  this.type = type;
  this.reset();
}

InstanceBrowser.prototype = Object.create(e2.EventEmitter2.prototype);
InstanceBrowser.prototype.constructor = InstanceBrowser;


InstanceBrowser.prototype.reset = function() {
  this.data = Object.create(null);
  this.emit('reset');
};


function key(name, type, domain)
{
  return `${name}.${type}.${domain}`;
}


InstanceBrowser.prototype.add = function(iface, proto, name, type, domain, flags) {
  dbg(name);

  let k = key(name, type, domain),
      cur = this.data[k];

  this.data[k] = this.data[k] || {};
  let v = this.data[k];
  v.name = name,
  v.type = type,
  v.domain = domain;

  if (!cur) {
    this.emit('add', k);
    dbg(`New service instance '${k}'`);
  }
};



InstanceBrowser.prototype.remove = function(iface, proto, name, type, domain, flags) {
  // FIXME
};


InstanceBrowser.prototype.start = async function() {
  try {
    this.browser = await create_browser(this.type, (...args) => this.add(...args), (...args) => this.remove(...args));
  } catch(e) {
    dbg(`Error while creating service browser: ${e}`);
    throw e;
  }
};


InstanceBrowser.prototype.stop = function() {
  dbg(`Stopping service instance browser for ${this.type}`);
  this.browser.Free(() => {
    dbg(`Service instance browser stopped`);
  });
  delete this.browser;
  this.reset();
};


InstanceBrowser.prototype.toGraph = function(root) {
  let n = Object.keys(this.data).map(k => {
    let uri = new ResourceURI('sece', root.vertices[0].uri.node, 'srv/' + encodeURIComponent(k));
    return service2node(uri, this.data[k]);
  });
  return {
    vertices: n,
    edges: n.map(n => {
      return {
	source: root.vertices[0].uri.toString(),
	target: n.uri.toString(),
	type: 'zeroconf'
      };
    })
  };
};
