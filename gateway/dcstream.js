import util   from 'util';
import stream from 'stream';

const {Duplex} = stream;

export function DCStream(channel, options)
{
  let opts = {
    decodeStrings: true,
    ...options
  };

  if (!(this instanceof DCStream))
    return new DCStream(channel, opts);

  Duplex.call(this, opts);

  this.rx_queue = [];
  this.reading = false;

  this.channel = channel;
  this._bind_datachannel();
  channel.binaryType = 'arraybuffer';

  // We need to listen for finish events in order to close the data channel
  // once .end has been called on this stream. Otherwise, the data channel
  // would be left open indefinititely and the remote end would not be able to
  // detect that the upstream connection (piped) has cleanly closed. The
  // _write method does NOT get the null chunk in this case for some reason.
  this.once('finish', () => {
    this.close();
  });

  return this;
};


DCStream.prototype._push_item = function(item) {
  this.rx_queue.push(item);

  // Emit a readable event only if we are transitioning from an empty queue to
  // a non empty queue.
  if (this.rx_queue.length == 1) {
    this.emit('readable');
  }
  if (this.reading) {
    this._rx_data();
  }
};


DCStream.prototype._bind_datachannel = function() {
  this.channel.onmessage = event => {
    return this._push_item(event.data);
  };
  this.channel.onclose = () => {
    this._push_item(null);
    this.close();
  };
  this.channel.onerror = event => {
    this.emit('error', event);
    this.close();
  };
};


DCStream.prototype._unbind_datachannel = function() {
  this.channel.onmessage = undefined;
  this.channel.onclose   = undefined;
  this.channel.onerror   = undefined;
};


DCStream.prototype._rx_data = function() {
  let d;

  while (this.rx_queue.length > 0) {
    d = this.rx_queue.shift();

    if (d instanceof ArrayBuffer)
      d = Buffer.from(d);

    if (d != null && typeof d !== 'string' && !(d instanceof Buffer)) {
      let e = new Error('Unsupported DataChannel message format');
      e.data = d;
      throw e;
    }

    this.reading = this.push(d);
    if (!this.reading) break;
  }
};


DCStream.prototype._read = function(n) {
  let d;

  this.reading = true;
  this._rx_data();
};


DCStream.prototype.close = function() {
  if (this.channel) {
    this._unbind_datachannel();
    this.channel.close();
    delete this.channel;
  }
};


DCStream.prototype._write = function(d, enc, cb) {
  if (d instanceof Buffer) {
    //d = d.buffer.slice(d.byteOffset, d.byteOffset + d.byteLength);

    // FIXME: Find better way to detect when to convert Buffers to strings,
    // ideally this would be perfomed by another element in the pipeline.
    d = d.toString();
  }

  try {
    this.channel.send(d);
    cb(null);
  } catch(e) {
    cb(e);
  }
};


util.inherits(DCStream, Duplex);
