import debug       from 'debug';
import e2          from 'eventemitter2';
import {GUID}      from '@sece/lib';
import {DCStream}  from './dcstream.js';


const dbg = debug('sece:proxy');


export class Manager extends e2.EventEmitter2 {
  constructor() {
    super();
    this.splices = {};
  }

  createSplice(...args) {
    let s = new Splice(...args);
    this.splices[s.id.toString()] = s;
    setImmediate(() => this.emit('create', s));

    s.once('end', () => {
      if (!this.splices[s.id.toString()]) return;

      dbg('Destroying splice');
      delete this.splices[s.id.toString()];
      this.emit('destroy', s);
    });

    return s;
  }
}


export class Splice extends e2.EventEmitter2 {
  constructor(channel, socket) {
    super();
    this.id = GUID.random();

    this.channel = channel;
    this.channel_stream = new DCStream(channel);
    this.socket = socket;

    const terminate = () => this.emit('end');
    this.socket.once('end', terminate);
    this.channel_stream.once('end', terminate);

    this.socket.once('error', () => this.emit('error'));
    this.channel_stream.once('error', () => this.emit('error'));
  }

  pipe() {
    this.dbg('Splicing DataChannel and socket');
    this.channel_stream.pipe(this.socket).pipe(this.channel_stream);

    this.channel_stream.once('error', () => {
      this.dbg('Error on DataChannel, closing socket');
      this.socket.end();
    });

    this.socket.once('error', () => {
      this.dbg('Error on socket, closing DataChannel');
      this.channel_stream.close();
    });

    setImmediate(() => this.emit('pipe'));
  }

  dbg(m) {
    return dbg(`[${this.id}] ${m}`);
  }
}
