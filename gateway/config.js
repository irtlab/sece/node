import debug            from 'debug';
import http             from 'http';
import {
  http_upgrade_service,
  tcp_server_service,
  tcp_client_service}   from './service.js';

const dbg = debug('sece:proxy');


// Given an array of services, stop them all. The returned promise resolves
// once all the services have been stopped.
// Cleanly shutdown all already started services. Returns a promise which
// resolves once all the services have been stopped and use no resources
// anymore.
function stop_services(srv)
{
  return new Promise(function(resolve, reject) {
    let remaining = srv.length;

    if (remaining == 0) {
      resolve();
      return;
    }

    const done = () => (--remaining == 0) ? resolve() : undefined;
    srv.map(s => s.destroy().then(done, e => {
      dbg(`Error while stopping service: ${e}`);
      done();
    }));
  });
}


// Given an array of service descriptor objects in srv, atomically start all
// the services. After the returned promise has resolved or rejected, either
// all or none of the services will have been started. When resolved, the
// promise returns an array of started service objects.
//
export function start_services(srv, rtc_peer, splice_manager)
{
  let p = [];

  for(var s of srv) {
    if      (s.type == 'http.server') p.push(http_upgrade_service(s, rtc_peer, splice_manager));
    else if (s.type == 'tcp.server')  p.push(tcp_server_service(s, rtc_peer, splice_manager));
    else if (s.type == 'tcp.client')  p.push(tcp_client_service(s, rtc_peer, splice_manager));
    else throw new Error(`Unsupported service type '${s.type}'`);
  }

  // If any of the promises rejected, close all the services that
  // successfully opened before throwing an error.
  let rv = new Promise(function(resolve, reject) {
    let started = [],
	errors = [],
	remaining = p.length;

    if (remaining == 0) {
      resolve([]);
      return;
    }

    // Attempt to start all services. If any of the services fails to start,
    // cleanly shutdown all already started services before resolving the
    // returned promise. If one or more services fail to start, the the
    // returned promise will be rejected after all already started services
    // have been stopped. Thus, the code either starts all or none of the
    // services and the promise resolves/rejects after all async operations
    // have been completed.
    p.map(v => {
      v.then(s => started.push(s), e => errors.push(e))
       .then(() => {
	 if (--remaining) return;
	 if (started.length == p.length) resolve(started);
	 else stop_services(started).then(() => {
	   let e = new Error('One or more services failed to start');
	   e.errors = errors;
	   reject(e);
	 });
       });
    });
  });

  return rv;
}


// Apply the new service configuration, disrupting as few services as
// possible. If an existing service with exactly the same configuration is
// found, it is reused. New services are started and old services not present
// in the new configuration are stopped.
export function apply_config(new_, old, rtc_peer, splice_manager)
{
  let rv = {},
      start = [];

  old = old || [];
  const running = old.reduce((a, s) => {
    a[s.config] = s;
    return a;
  }, {});

  for(var srv of new_) {
    // TODO: Ideally, we would sort the keys in the object to get a stable
    // string representation of the object. This way, changing the order of
    // keys in the service's configuration will result in the service being
    // restarted (which is not necessary).
    let cfg = JSON.stringify(srv);

    if (running[cfg]) {
      rv[cfg] = running[cfg];
      delete running[cfg];
    } else {
      start.push(srv);
    }
  }

  let stop = [];
  for(var id in running)
    stop.push(running[id]);

  return stop_services(stop).then(() => start_services(start, rtc_peer, splice_manager).catch(() => {
    let start = stop.map(s => JSON.parse(s.config));
    return start_services(start, rtc_peer, splice_manager).then(() => {
      throw new Error('New service configuration failed to start');
    }, e => {
      let err = new Error('Failed to revert to previous service configuration');
      err.reason = e;
      throw err;
    });
  }));
}
