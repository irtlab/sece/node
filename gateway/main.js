// There appear to be only two working WebRTC modules for Node.js:
//  [1] https://github.com/vmolsa/webrtc-native
//  [2] https://github.com/js-platform/node-webrtc
//
// [1] embeds Chromium code/library and supports both data channels and media
// channels (without the producer consumer part, only the APIs). [2] Relies on
// a pre-compiled version of the libwebrtc library and appears to crash node
// sometimes.

// Converting WebRTC data channel into stream
// - not really doable since the DC API do not provide support for backpressure
// - which is needed when piping a DC into a TCP stream
// - Also, in SCTP all streams within an association share congestion control,
//   which means one blocked stream will block other streams within the same
//   association. This is by design (to make it TCP friendly).
// - This is not a problem in the browser where the producer/consumer
//   connected to the stream is the same JS process. There, the process is
//   blocked, processing the data, and thus wont pick up data from SCTP
//   buffers. This will be signaled to the remote end via increasing amount of
//   buffered data in the data stream.

// FIXME: DataChannel is not disconnected when ICE fails.

import stdio                  from 'stdio';
import debug                  from 'debug';
import {
  WebSocketEmitter,
  ReconnectingWebSocket}      from '@sece/lib';
import {NodeURI, ResourceURI} from '@sece/lib';
import {Peer, attach_to_ws}   from '@sece/lib';
import {RTCPeer}              from '@sece/lib';

//import {get_access_token}     from 'sece/web/oauth';
//import {keychain}             from 'sece/web/auth/keychain';

import {Manager}              from './splice.js';
import {apply_config,
	start_services}       from './config.js';
import {graph_subscribe}      from './graph.js';
import {InstanceBrowser}      from './zeroconf.js';
import {zeroconf_connect}     from './service.js';


const dbg = debug('sece:gateway');


global.opts = stdio.getopt({
  'min_port': {key: 'm', args: 1, description: 'Minimum port number to allocate to services'},
  'max_port': {key: 'M', args: 1, description: 'Maximum port number to allocate to services'}
});

opts.min_port = opts.min_port || 50000;
opts.max_port = opts.max_port || 60000;


let uri = localStorage.getItem('gateway.uri');
try {
  uri = NodeURI.parse(uri.trim());
} catch(e) {
  uri = NodeURI.random();
  localStorage.setItem('gateway.uri', uri.toString());
}
dbg(`Starting SECE WebRTC Gateway ${uri}`);


dbg(`Establishing a WebSocket connection to ${config['WS_SERVER_URI']}`);
let ws             = new ReconnectingWebSocket(config['WS_SERVER_URI']),
    sock           = new WebSocketEmitter(ws),
    splice_manager = new Manager(),
    services;


export const root_graph = {
  vertices: [{
    uri: uri,
    label: 'Gateway',
    type: ['peer', 'gateway']
  }],
  edges: []
};


export const service_types = {
  '_sece._tcp':        new InstanceBrowser('_sece._tcp'),
  '_smob._tcp':        new InstanceBrowser('_smob._tcp'),
  //  '_http._tcp':        new InstanceBrowser('_http._tcp'),
  '_ipp._tcp':        new InstanceBrowser('_ipp._tcp'),
  '_datachannel._tcp': new InstanceBrowser('_datachannel._tcp')
};


export const channel_map = {
  '/.well-known/graph': graph_subscribe,
  'srv/':              zeroconf_connect
};


const default_config = [{
  type: 'http.server',
  port: 50000,
  advertise: true
}, {
  type: 'tcp.client'
}];


function on_data(sel, c) {
  let channel;

  Object.keys(channel_map).some(p => {
    if (sel.target.startsWith(p)) {
      channel = channel_map[p](sel, c, splice_manager);
      return true;
    };
    return false;
  });

  return channel;
};


async function main() {
  let signaling_peer = new Peer(uri, {
    register: true,
    keychain: keychain,
    scope: 'signaling'
  });
  attach_to_ws(signaling_peer, sock);

  // FIXME: Pass rtc_config to RTCPeer
  let rtc_peer = new RTCPeer(signaling_peer);
  rtc_peer.on_data_request = on_data;
  rtc_peer.listen();

  apply_config(default_config, services, rtc_peer, splice_manager).then(v => {
    services = v;
  }).catch(e => {
    console.log(`error while creating services: ${e}`);
    throw e;
  });
}

main();
