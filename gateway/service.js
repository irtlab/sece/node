import debug         from 'debug';
import http          from 'http';
import net           from 'net';
import url           from 'url';
import {NodeURI, ResourceURI} from '@sece/lib';
//import mdns          from 'mdns';
import {channel_map} from './main.js';
import {resolve_srv} from './zeroconf.js';

const dbg = debug('sece:gateway');


function net_server(config, s, ad)
{
  let obj = {
    config: JSON.stringify(config),
    server: s,
    connections: {}
  };

  if (ad) obj.advertisement = ad;

  // Keep track of active connections so that we can destroy them when
  // destroying the server, e.g., on configuration reload.
  s.on('connection', c => {
    let id = `${c.remoteFamily}:${c.localAddress}:${c.localPort}:${c.remoteAddress}:${c.remotePort}`;
    obj.connections[id] = c;
    c.once('close', () => {
      delete obj.connections[id];
    });
  });

  obj.destroy = () => {
    if (obj.advertisement) obj.advertisement.stop();
    return new Promise(function(resolve, reject) {
      s.once('close', resolve);
      s.close();
      for(var id in obj.connections)
	obj.connections[id].destroy();
    });
  };

  return obj;
}


function connect(uri) {
  return new Promise(function(resolve, reject) {
    dbg(`Connecting to ${uri.hostname}:${uri.port}`);
    let s = net.connect(parseInt(uri.port), uri.hostname);

    s.once('error', e => {
      let code = 500,
	  reason = 'Server Internal Error';

      switch(e.errno) {
      case 'ETIMEDOUT':
	code = 504;
	reason = 'Connection Timeout';
	break;

      case 'ECONNREFUSED':
      case 'ECONNABORTED':
      case 'ECONNRESET':
	code = 503;
	reason = 'Service Unavailable';
	break;
      }

      let error = new Error(reason);
      error.code = code;
      error.reason = reason;
      dbg(`Connection failed: ${e.errno}`);
      reject(error);
    });

    s.once('connect', () => {
      dbg(`Connected to ${uri.hostname}:${uri.port}`);
      s.removeAllListeners('error');
      s.setNoDelay(true);
      resolve(s);
    });

    s.once('close', () => {
      dbg(`Disconnected from ${uri.hostname}:${uri.port}`);
    });
  });
}


function sock_client(splice_manager, selector, d) {
  return connect(url.parse(selector.target))
    .then(sock => {
      let c = d.rtc.connection.createDataChannel(selector.options.label, {
	negotiated: true,
	...selector.options
      });

      splice_manager.createSplice(c, sock);
      return c;
    });
}


function terminate(req, sock, code, reason)
{
  sock.write(
    `HTTP/1.1 ${code} ${reason}\r\n` +
    'Connection: close\r\n' +
    'Content-Length: 0\r\n' +
    '\r\n');
  req.destroy();
};


function upgrade(rtc_peer, splice_manager, req, sock)
{
  const up = req.headers['upgrade'];

  if (up !== 'DataChannel') {
    terminate(req, sock, 501, 'Unsupported Upgrade Protocol');
    return;
  }

  let u;
  try {
    u = ResourceURI.parse(req.url);
  } catch(e) {
    terminate(req, sock, 400, 'Invalid Resource URI');
    return;
  }

  if (!u.resource) {
    terminate(req, sock, 400, 'Invalid or Missing Resource ID');
    return;
  }

  rtc_peer.connect(u.node)
    .then(d => d.dialog.rtc.sync_data(u.resource))
    .then(c => {
      sock.write('HTTP/1.1 101 Switching Protocols\r\n' +
		 'Upgrade: DataChannel\r\n' +
		 'Connection: Upgrade\r\n' +
		 '\r\n');

      splice_manager.createSplice(c, sock);
    })
    .catch(e => {
      console.log(e, e.stack);
      terminate(req, sock, 500, "DataChannel Creation Failed");
      return;
    });
}


export function http_upgrade_service(config, rtc_peer, splice_manager)
{
  let {port, timeout} = config;

  port = parseInt(port);
  timeout = timeout || 0;

  return new Promise(function(resolve, reject) {
    dbg(`Starting HTTP Upgrade service on port ${port}`);
    let s = http.createServer((req, res) => {
      res.statusCode = 404;
      res.end();
    });
    s.timeout = timeout;

    s.once('error', e => {
      dbg(`HTTP Upgrade service on port ${port} failed to start: ${e}`);
      reject(e);
    });

    s.once('close', () => {
      dbg(`Closing HTTP Upgrade service on port ${port}`);
    });

    s.listen(port, () => {
      dbg(`HTTP Upgrade service listening on port ${port}`);
      s.removeAllListeners('error');
      s.on('error', e => console.log(e, e.stack));
      s.on('upgrade', (...args) => upgrade(rtc_peer, splice_manager, ...args));

      let ad;
      if (config.advertise) {
	let uri = NodeURI.parse(rtc_peer.uri);
	// ad = mdns.createAdvertisement(mdns.tcp('sece'), port, {
	//   name: `SECE Gateway [${uri.node.toString()}]`,
	//   txtRecord: {
	//     node: uri.toString()
	//   }
	// });
	// ad.start();
      }

      resolve(net_server(config, s, ad));
    });
  });
}


export function tcp_server_service(config, rtc_peer, splice_manager)
{
  let {port, uri} = config;
  port = parseInt(port);

  return new Promise(function(resolve, reject) {
    dbg(`Starting TCP->DataChannel service on port ${port}`);
    let s = net.createServer({pauseOnConnect: true});

    if (typeof uri === 'undefined')
      throw new Error('Invalid or missing resource URI');

    uri = ResourceURI.parse(uri);

    if (!uri.resource)
      throw new Error('Invalid or missing resource ID');

    s.once('error', e => {
      dbg(`TCP->DataChannel service on port ${port} failed to start: ${e}`);
      reject(e);
    });

    s.once('close', () => {
      dbg(`Closing TCP->DataChannel service on port ${port}`);
    });

    s.listen(port, () => {
      s.removeAllListeners('error');

      dbg(`TCP->DataChannel service listening on port ${port}, forwarding to ${uri}`);
      s.on('error', e => console.log(e, e.stack));
      s.on('connection', sock => {
	rtc_peer.connect(uri.node)
	  .then(d => d.dialog.rtc.sync_data(uri.resource))
	  .then(c => splice_manager.createSplice(c, sock))
	  .then(() => sock.resume())
	  .catch(e => sock.end());
      });

      let ad;
      if (config.advertise) {
	let node = NodeURI.parse(rtc_peer.uri);
	ad = mdns.createAdvertisement(mdns.tcp('datachannel'), port, {
	  name: `SECE Gateway [${node.node.toString()}] ${port}`,
	  txtRecord: {
	    node: node.toString(),
	    remote: uri.toString()
	  }
	});
	ad.start();
      }

      resolve(net_server(config, s, ad));
    });
  });
}


export function tcp_client_service(config, rtc_peer, splice_manager) {
  if (channel_map['tcp:'] || channel_map['udp:']) {
    return Promise.reject(new Error('Resource is busy'));
  }

  dbg('DataChannel->TCP service listening on signaling channel');

  let f = (...args) => sock_client(splice_manager, ...args);
  channel_map['tcp:'] = f;
  channel_map['udp:'] = f;

  return Promise.resolve({
    key: JSON.stringify(config),
    destroy: () => {
      dbg('Closing DataChannel->TCP service');
      delete channel_map['tcp:'];
      delete channel_map['udp:'];
      return Promise.resolve();
    }
  });
}


export async function zeroconf_connect(sel, c, splice_manager)
{
  dbg('ZeroConf connect');
  let dc = c.connection.createDataChannel(sel.options.label, {
    negotiated: true,
    ...sel.options
  });

  let srv = decodeURIComponent(sel.target).slice(4);
  let ss = srv.split('/')[0];
  dbg(`Resolving ZeroConf service '${ss}'`);

  let {addr, port} = await resolve_srv(ss);

  let s = await connect({hostname: addr, port: port});
  let spl = splice_manager.createSplice(dc, s);
  spl.pipe();

  spl.on('error', e => console.log(e));

  // dc.onclose = () => {
  //   dbg('ZeroConf client closed datachannel');

  //   dc.onopen = null;
  //   dc.onmessage = null;
  //   dc.onclose = null;

  //   dc = null;
  // };

  return dc;
}
