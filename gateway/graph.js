/**
 * @module sece/gateway/graph
 *
 * Network and service discovery for the SECE WebRTC gateway. The
 * network discovery service uses a ZeroConf/Bonjour client to
 * discover nodes and services available in the local network. It then
 * makes the information available to the rest of the SECE network in
 * form of a resource graph.
 */
import _                           from 'underscore';
import debug                       from 'debug';
import {root_graph, service_types} from './main.js';
import {InstanceBrowser}           from './zeroconf.js';

const dbg = debug('sece:gateway:graph');

let listeners = 0;


function start_browsing() {
  Object.values(service_types).forEach(s => s.start());
}


function stop_browsing() {
  Object.values(service_types).forEach(s => s.stop());
}


function build_update() {
  let g = {
    vertices: [...root_graph.vertices],
    edges: [...root_graph.edges]
  };

  Object.values(service_types).forEach(s => {
    let {vertices, edges} = s.toGraph(root_graph);
    g.vertices = g.vertices.concat(vertices);
    g.edges = g.edges.concat(edges);
  });

  return g;
}


export function graph_subscribe(sel, c)
{
  dbg('Graph subscribe request');
  let dc = c.connection.createDataChannel(sel.options.label, {
    negotiated: true,
    ...sel.options
  });

  if (!listeners++) {
    dbg('Starting ZeroConf browsers');
    start_browsing();
  }

  // FIXME: A temporary fix to ensure that the first update is received after
  // the remote party has installed its onmessage handler. This should be
  // fixed in the signaling library.
  // FIXME: There is a race condition in data channel setup. When a client
  // sends a SYN request, it gets the connection after the transaction has
  // been confirmed. Only at that point it can setup the onmessage callback.
  // If the server sends a message before that, the message will be lost by
  // the client.

  let f = _.throttle(() => dc && dc.send(JSON.stringify(build_update())), 500);
  setTimeout(f, 100);
  Object.values(service_types).forEach(s => s.onAny(f));

  dc.onclose = () => {
    dbg('Graph subscriber closed channel');

    dc.onopen = null;
    dc.onmessage = null;
    dc.onclose = null;

    dc = null;
    Object.values(service_types).forEach(s => s.offAny(f));

    //if (!--listeners) {
      //dbg('Last subscriber quit, stopping ZeroConf browsers');
      //stop_browsing();
   // }
  };

  return dc;
}
