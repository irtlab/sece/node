import debug           from 'debug';
import _               from 'underscore';
import {EventEmitter2} from 'eventemitter2';
import mdns            from 'mdns';

const dbg = debug('sece:gw:test');

// Discover the set of advertised service types on the local network:
// _services._dns-sd._udp.local     PTR _http._tcp.local.

// Discover the list of instances that provide a certain service:
// _http._tcp.local.                PTR SomeUniqueName._http._tcp.local.

// SomeUniqueName._http._tcp.local. SRV 0 0 5060 rimmer.local.


// var sequence = [
//     mdns.rst.DNSServiceResolve(),
//     'DNSServiceGetAddrInfo' in mdns.dns_sd ? mdns.rst.DNSServiceGetAddrInfo() : mdns.rst.getaddrinfo({families:[0]}),
//     mdns.rst.makeAddressesUnique()
// ];
// var browser = mdns.createBrowser(mdns.tcp('http'), {resolverSequence: sequence});


let service_types = new ServiceRegistry();

var s = mdns.browseThemAll();

s.on('serviceUp', s => service_types.add(s));
s.on('serviceDown', s => service_types.remove(s));

// We need to register an error callback, otherwise the mdns library might
// crash Node when it encounters an error.
s.on('error', e => {
  console.log(e, e.stack);
});

s.start();


service_types.onAny(_.debounce(() => {
  let graph;

  console.log(JSON.stringify(service_types.toGraph()));
}, 500));
