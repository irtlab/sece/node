#!/bin/bash
set -Eeuo pipefail

# Create an empty image with a layout similar to the Raspberry OS image. The
# parameters of the layout (partitions) can be customized through the
# configuration file.

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

res=$(ensure_tools dd sudo sed parted losetup mkfs.ext4) || {
    bail "Missing commands: $res"
}

[ "$#" -eq 1 ] || {
    bail "Usage: $0 <image>"
}

image="$1"

info "Creating empty disk image $(basename \"$image\") of size $disk_size..."
dd if=/dev/zero of="$image" bs=1 count=0 seek=$disk_size status=progress
parted -s "$image" mklabel msdos

info "Creating boot and rootfs partitions in $(basename \"$image\")"
parted -s "$image" mkpart primary ext4 $boot_partition_begin $boot_partition_end
parted -s "$image" mkpart primary ext4 $rootfs_partition_begin $rootfs_partition_end

trap detach_image EXIT
attach_image "$image"

info "Initializing boot and rootfs filesystems in $(basename \"$image\")"
sudo mkfs.ext4 -F -L boot   "${disk}p1"
sudo mkfs.ext4 -F -L rootfs "${disk}p2"

info "done."