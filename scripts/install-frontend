#!/bin/bash
set -Eeuo pipefail

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info "Installing web frontend"

install -o www-data -g www-data -m 0644 /dev/stdin /var/www/html/.index.html.sece << "EOF"
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ it.name }}</title>
        <style>
            body, html {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
            }
            body {
                display: flex;
                flex-direction: column;
            }
            .navbar {
                padding: 0 !important;
            }
            #content {
                flex-grow: 1;
            }
        </style>
        <link href="https://cdn.sece.io/bootstrap/5.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.sece.io/bootstrap/5.1.3/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">{{ it.name }}</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMenu">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" href="/">Graphs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/control">Control</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/programs">Programs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/data">Data</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="https://vouch.{{ it.domains[0] }}/logout">Sign out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <iframe id="content" allow="fullscreen 'src'; geolocation 'src'">
        </iframe>
        <script>
            let src;
            const proto = window.location.protocol;
            const host = window.location.host;
            switch(window.location.pathname.split('/')[1]) {
                case 'graphs'   : src = `${proto}//grafana.${host}`   ; break;
                case 'control'  : src = `${proto}//nodered.${host}/ui`; break;
                case 'programs' : src = `${proto}//nodered.${host}`   ; break;
                case 'data'     : src = `${proto}//pgadmin.${host}`   ; break;
                default         : src = `${proto}//grafana.${host}`   ; break;
            };
            document.getElementById('content').src = src;
        </script>
    </body>
</html>
EOF
register_template /var/www/html/index.html