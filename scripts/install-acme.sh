#!/bin/bash
set -Eeuo pipefail

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info "Installing acme.sh"

cd /usr/local/src
git clone https://github.com/acmesh-official/acme.sh.git
cd acme.sh

./acme.sh                   \
    --install               \
    --home /etc/acme.sh     \
    --accountemail "$email" \
    --nocron

cat > /etc/systemd/system/acme.sh.timer << "EOF"
[Unit]
Description=Run acme.sh certificate renewal once a day

[Timer]
OnCalendar=*-*-* 00:42:00

[Install]
WantedBy=timers.target
EOF

cat > /etc/systemd/system/acme.sh.service << "EOF"
[Unit]
Description=acme.sh certificate renewal

[Service]
Type=oneshot
Environment=CERT_HOME=/data/acme.sh
ExecStart=/etc/acme.sh/acme.sh --home /etc/acme.sh --cron
PrivateTmp=true
EOF

cat > /etc/acme.sh/account.conf << EOF
#LOG_FILE="/etc/acme.sh/acme.sh.log"
#LOG_LEVEL=1

#AUTO_UPGRADE="1"

#NO_TIMESTAMP=1

ACCOUNT_EMAIL='$email'
SAVED_ACMEDNS_UPDATE_URL='https://auth.acme-dns.io/update'
USER_PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
EOF

systemctl daemon-reload
systemctl enable acme.sh.timer
