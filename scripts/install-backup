#!/bin/bash
set -Eeuo pipefail

# How to configure Duplicity backup to a Google Cloud Storage bucket
#
# Create a new Google Cloud Storage bucket. Set storage location type to
# multi-region, pick your storage region, and set storage class to Nearline.
# Enable public access prevention.
#
# Create a lifecycle rule to delete objects that have been modified 100+ days
# ago. This will keep three months worth of backups in the bucket, which should
# be sufficient.
#
# In IAM & Admin > Service Accounts create a new service account. Assign the
# service account to Storage Object Creator and Storage Object Viewer roles. Add
# conditions to both roles to restrict access only to the backup buckets:
# resource.name.startsWith("projects/_/buckets/sece-node-backup-")
#
# In Cloud Storage > Settings > Interoperability select "create a key for a
# service account". Select your recently created service account. Write down the
# access key and secret.

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info "Installing the backup script"

apt install --no-install-recommends -y git duplicity python3-boto

install -o root -g root -m 0755 /dev/stdin /usr/local/sbin/backup << "EOF"
#!/bin/bash
set -eEuo pipefail

env="/etc/duplicity/env.sh"

[ $(whoami) == 'root' ] || {
    echo "This script must be run under root"
    exit 1
}

[ -r "$env" ] || {
    echo "Cannot read environment file $env"
    exit 1
}

# Run duplicity to encrypt data and then copy to GCS.
#
# Always use --archive-dir /var/cache/duplicity/ in your Duplicity commands.
# This makes sure that Duplicity uses the same local cache even if another user
# runs Duplicity commands. If you don’t do this, Duplicity will rebuild the local
# cache by downloading files from the cloud storage.
#

source "$env"

echo "### Running duplicity at $(date)"

mkdir -p /data/tmp
duplicity                                  \
    --archive-dir /var/cache/duplicity     \
    --tempdir /data/tmp                    \
    --full-if-older-than 1M                \
    --volsize 256                          \
    --exclude '/etc/duplicity'             \
    --exclude '/home/*/.cache'             \
    --exclude '/home/*/.npm'               \
    --exclude '/home/*/.vscode-server'     \
    --exclude '/root/.cache'               \
    --exclude '/root/.npm'                 \
    --exclude '/root/.vscode-server'       \
    --exclude '/root/.gnupg'               \
    --exclude '/var/backups'               \
    --exclude '/var/cache/duplicity'       \
    --exclude '/var/lib/docker'            \
    --exclude '/var/lock'                  \
    --exclude '/var/run'                   \
    --exclude '/var/tmp'                   \
    --exclude '/var/swap'                  \
    --include '/data/**'                   \
    --include '/boot/**'                   \
    --include '/etc/**'                    \
    --include '/home/**'                   \
    --include '/root/**'                   \
    --include '/srv/**'                    \
    --include '/usr/local/**'              \
    --include '/var/**'                    \
    --exclude '**'                         \
    /                                      \
    "gs://$GS_BUCKET/$GS_BUCKET_DIR"

# Show collection status
#duplicity --archive-dir /var/cache/duplicity collection-status gs://$GS_BUCKET/$GS_BUCKET_DIR

# Restore files
#duplicity --archive-dir /var/cache/duplicity restore gs://$GS_BUCKET/$GS_BUCKET_DIR /restored
EOF

mkdir -p /etc/duplicity
install -o root -g root -m 0600 /dev/stdin /etc/duplicity/.env.sh.sece << EOF
export GS_ACCESS_KEY_ID=GOOG1ENGZIIUSGQC236JNTNAJPGMHW5TPMCKBKIW6BFNJL3V3G3MU34VZDA3I
export GS_SECRET_ACCESS_KEY=ETQUmqincsX9irQTgDtZZPtaXo9oVkhT2cFhAwBh
export GS_BUCKET="sece-node-backup-{{ it.backup.region }}"
export GS_BUCKET_DIR="{{ it.id }}"
export PASSPHRASE="{{ it.backup.password }}"
EOF
register_template /etc/duplicity/env.sh

cat > /etc/systemd/system/backup.service << EOF
[Unit]
Description=Backup to Google Cloud

[Service]
Type=oneshot
ExecStart=/usr/local/sbin/backup
PrivateTmp=true
EOF

cat > /etc/systemd/system/backup.timer << EOF
[Unit]
Description=Run the backup service once a day

[Timer]
OnCalendar=*-*-* 04:50:00
RandomizedDelaySec=43200
Persistent=true

[Install]
WantedBy=timers.target
EOF

systemctl enable backup.timer
