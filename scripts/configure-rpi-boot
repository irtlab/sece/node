#!/bin/bash
set -Eeuo pipefail

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info -n "Switching Raspberry Pi boot process to LABELS..."

# Find the root partition by label rather than by PARTUUID. This enables us to
# switch root partitions on OS update by reconfiguring the labels.
#sed -i 's|root=[^ ]\+|root=LABEL=rootfs|' /boot/cmdline.txt

# Disable the one-time boot script used by RaspiOS. We do not resize the root
# partition. Instead, we create new partitions from systemd.
sed -i 's| init=/usr/lib/raspi-config/init_resize\.sh||' /boot/cmdline.txt

# The following snippets are taken from /usr/lib/raspi-config/init_resize.sh
sed -i 's| sdhci\.debug_quirks2=4||' /boot/cmdline.txt
if ! grep -q splash /boot/cmdline.txt; then
    sed -i "s/ quiet//g" /boot/cmdline.txt
fi

# Reconfigure /etc/fstab to find the boot and root partitions by LABEL
sed -i 's|^[^ \t]\+[ \t]\+/boot[ \t]|LABEL=boot  /boot |' /etc/fstab
sed -i 's|^[^ \t]\+[ \t]\+/[ \t]|LABEL=rootfs  / |' /etc/fstab

info "done."

info "Mounting /data partition..."

mkdir /data
cat >> /etc/fstab << EOF

LABEL=data    /data    ext4    defaults,noatime  0    2
EOF

info "done."

# Create a script that can be invoked from systemd to create the alternate
# rootfs and data partitions on the remaining SD card disk space.
install -o root -g root -m 0755 /dev/stdin /usr/local/sbin/create-partitions << EOF
#!/bin/bash
set -Eeuo pipefail

disk=/dev/mmcblk0

if [ ! -b "\${disk}p3" ] ; then
    echo "### Resizing rootfs partition"
    parted -s "\$disk" resizepart 2 8921087s
    resize2fs "\${disk}p2"

    echo "### Creating alternate rootfs partition"
    parted -s "\$disk" mkpart primary ext4 8921088s 17309695s
    mkfs.ext4 -F -L _rootfs "\${disk}p3"
    partprobe "\$disk"
fi

if [ ! -b "\${disk}p4" ] ; then
    echo "### Creating data partition"
    parted -s \$disk mkpart primary ext4 17309696s 100%
    mkfs.ext4 -F -L data "\${disk}p4"
    partprobe "\$disk"
fi
EOF

cat > /etc/systemd/system/create-partitions.service << EOF
[Unit]
Description=Create alternate rootfs and data partitions if necessary
Before=local-fs-pre.target
DefaultDependencies=no

[Service]
Type=oneshot
ExecStart=/usr/local/sbin/create-partitions

[Install]
WantedBy=local-fs-pre.target
EOF
systemctl enable create-partitions
