#!/bin/bash
set -Eeuo pipefail

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info "Installing LoRa gateway"
apt install --no-install-recommends -y git

groupadd --system --gid 720 lora
useradd --system --uid 720 --gid 720 --no-log-init --home /homoe/lora --no-create-home --shell /usr/sbin/nologin lora

cd /usr/local/src
rm -rf lora-gateway

git clone https://github.com/janakj/lora-gateway
cd lora-gateway
npm install
npm run build

cd ..
chown -R $username:$username lora-gateway

cat > /etc/systemd/system/lora-gateway.service <<EOF
[Unit]
Description=LoRa gateway
StartLimitIntervalSec=0
Requires=mosquitto.service
After=mosquitto.service
RequiresMountsFor=/data/lora-gateway

[Service]
Type=simple
Restart=always
RestartSec=5
WorkingDirectory=/usr/local/src/lora-gateway
Environment=DEBUG=lora*
ExecStartPre=/usr/bin/mkdir -p /data/lora-gateway
ExecStartPre=/usr/bin/chown lora:lora /data/lora-gateway
ExecStart=node                                    \\
    --title=lora-gateway                          \\
    --es-module-specifier-resolution=node         \\
    dist/main.js                                  \\
        -d sqlite:/data/lora-gateway/state.db     \\
        --listen /run/lora-gateway.sock           \\
        --config /usr/local/etc/lora-gateway.json \\
        --mqtt_broker mqtt://localhost            \\
        --user lora                               \\
        --group lora

[Install]
WantedBy=multi-user.target
EOF

install -o lora -g lora -m 0644 /dev/stdin /usr/local/etc/lora-gateway.json << EOF
{
    "networks": {},
    "credentials": {}
}
EOF

systemctl enable lora-gateway