#!/bin/bash
set -Eeuo pipefail

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

info "Installing @sece/tool"

apt install --no-install-recommends -y git xz-utils whiptail pwgen

mkdir -p /usr/local/src/sece
cd /usr/local/src/sece
rm -rf tool

git clone https://gitlab.com/irtlab/sece/tool tool
cd tool
npm install
npm run build
npm link

cd ..
chown -R $username:$username tool

mkdir -p /etc/sece/templates

# Create a service to re-generate various configuration files from the SECE
# configuration file in /data/sece/config.json. The service must be started
# before the sysinit target so that daemons will have configuration files
# generated from templates. We only start it after the target local-fs to make
# sure all auto volumes have been mounted.
cat > /etc/systemd/system/sece-configure.service <<EOF
[Unit]
Description=Re-generate configuration files from SECE configuration
Before=sysinit.target
After=local-fs.target
RequiresMountsFor=/data/sece/config.json
ConditionPathExists=/data/sece/config.json
DefaultDependencies=no

[Service]
Type=oneshot
ExecStart=sece configure -s

[Install]
WantedBy=multi-user.target
EOF
systemctl enable sece-configure


# Create a script and a systemd service to re-configure the OS timezone based on
# the timezone value stored in the SECE configuration file on the data
# partition.
install -o root -g root -m 0755 /dev/stdin /usr/local/sbin/sece-set-timezone << "EOF"
#!/bin/bash

timezone="$(sece get timezone)"
tz_file="$(cat /etc/timezone 2>/dev/null)"

if [ -n "$timezone" -a "$timezone" != "$tz_file" ] ; then
    info "Setting timezone to $timezone"
    echo "$timezone" > /etc/timezone
    rm /etc/localtime
    ln -s "/usr/share/zoneinfo/$timezone" /etc/localtime
    dpkg-reconfigure -f noninteractive tzdata
fi
EOF

cat > /etc/systemd/system/sece-set-timezone.service <<EOF
[Unit]
Description=Set timezone from SECE configuration file
Before=sysinit.target
After=local-fs.target
RequiresMountsFor=/data/sece/config.json
ConditionPathExists=/data/sece/config.json
DefaultDependencies=no

[Service]
Type=oneshot
ExecStart=/usr/local/sbin/sece-set-timezone

[Install]
WantedBy=multi-user.target
EOF
systemctl enable sece-set-timezone


# Install a script to update the OS
install -o root -g root -m 0755 /dev/stdin /usr/local/sbin/update << "EOF"
#!/bin/bash
set -Eeuo pipefail
shopt -s dotglob

[ "$(whoami)" != "root" ] && {
    echo "This script must be run under root"
    exit 1
}

[ $? -ne 1 ] && {
    echo "Usage: $0 <tarball>"
    exit 1
}

mnt=/mnt

mount -L _rootfs $mnt
rm -rf $mnt/*

rootfs_cur=$(findmnt -nfo source /)
rootfs_new=$(findmnt -nfo source $mnt)

tar xfJ $1 -C $mnt

# Configure the machine-id over from the old filesystem to the new filesystem
cp -a /etc/machine-id $mnt/etc/machine-id

# Manually configure selected files from the boot partition until we figure out
# a better way to have alternate boot partitions (requires changes to the MBR or
# the sectors after the MBR that contain Grub's stage 1.5)
cp -a $mnt/boot/config-* /boot
cp -a $mnt/boot/initrd.img-* /boot
cp -a $mnt/boot/System.map-* /boot
cp -a $mnt/boot/vmlinuz-* /boot
cp -a $mnt/boot/grub/grub.cfg /boot/grub

umount $mnt

# Swap partition labels to make the OS boot from the newly installed partition
# on reboot.
e2label $rootfs_new rootfs
e2label $rootfs_cur _rootfs

echo "Please reboot now."
EOF
