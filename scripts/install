#!/bin/bash
set -Eeuo pipefail

export target

dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))
. $dir/../config.sh

[ "$(whoami)" = "root" ] || {
    bail "This script must be run under root."
}

# Basic platform setup
$dir/apt-upgrade
$dir/set-hostname
$dir/set-locale
$dir/set-keyboard
$dir/truncate-motd
$dir/create-minimal-target
$dir/reboot-on-kernel-panic

[ "$target" = "vm" ] && {
    $dir/configure-grub
#    $dir/install-vbox-guest
} || {
    $dir/configure-rpi-boot
    $dir/disable-power-led
}

# Install OpenSSH server for remote management
$dir/install-ssh

# Pre-requisites
$dir/install-docker
$dir/install-nodejs
$dir/install-golang
$dir/install-sece-tool
$dir/install-backup

# WireGuard for public access when behind NAT. This involves setting up various
# IPv6 services for access through the WireGuard tunnel.
$dir/install-wireguard
$dir/enable-ip-forwarding
$dir/configure-net-ifaces
$dir/install-radvd
$dir/install-tayga

# Install Mosquitto as the MQTT broker used for IPC on the node
$dir/install-mosquitto

# Gateways for various IoT technologies
$dir/install-zigbee2mqtt
$dir/install-hardwario
$dir/install-lora-gateway
#$dir/install-comet-gateway

# HTTP related services
$dir/install-acme.sh
$dir/install-nginx
$dir/install-vouch

# Install application services
$dir/install-postgres
#$dir/install-pusher
$dir/install-grafana
$dir/install-nodered
$dir/install-pgadmin
$dir/install-frontend
