#
# sece-node uses static uid/gid mappings to ensure that the /data
# partition can be used across various sece-node OS versions. The ranges are as
# follows:
#   * 100-399  : uid/gids added automatically by APT (packages that don't use /data)
#   * 400-699  : uid/gids added manually by the install scripts for APT packages that access /data
#     - 400    :   mosquitto
#     - 410    :   postgres
#     - 420    :   grafana
#   * 700-999  : uid/gids added for locally installed software (not from APT)
#     - 700    :   zigbee
#     - 710    :   hardwario
#     - 720    :   lora
#     - 730    :   nodered
#     - 740    :   pgadmin
#   * 1000-max : Ordinary user accounts
#     - 1000   :   sece/pi
#

raspios_image_url='https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf-lite.zip'
raspios_image_sha256='f6e2a3e907789ac25b61f7acfcbf5708a6d224cf28ae12535a2dc1d76a62efbc'

# Resize the disk image to the following size (qemu-img syntax). The value
# should be large enough to fit the boot and rootfs partitions whose parameters
# are configured below.
disk_size=5G

# An administrative email that several tools installed into the image need
email=sece@sece.io

# Configure the begin and end of the boot partition (parted syntax). Should be
# aligned with the placement of the boot partition in the Raspberry OS image so
# that the RPi and VM images have the same layout. These variables are only used
# during the creation of the VM image. The Raspberry OS image already contains a
# boot partition which will be reused unmodified. The partition should be
# aligned to the 4096 byte boundary for optimum performance.
boot_partition_begin=8192s
boot_partition_end=532479s

# Configure the begin and and of the rootfs partition (parted syntax). In the VM
# image the partition will be created. In the Raspberry OS image the existing
# rootfs will be extended to the end specified here. The partition should be
# aligned to the 4096 byte boundary for optimum performance.
rootfs_partition_begin=532480s
rootfs_partition_end=8921087s

# Mount partitions under the following directory
mnt='/mnt/sece'

# Hostname to set in the image
hostname='sece'

if [ "$target" = "rpi" ] ; then
    username=pi
    target_image=sece.rpi.armhf.img
elif [ "$target" = "vm" ] ; then
    username=sece
    password=$username
    target_image=sece.vm.amd64.img
else
    bail "Unsupported target $target"
fi

# Define variable root if it hasn't been defined already. Point it to the
# directory that contains the configuration file.
root=${root:-$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))}

# Store copies of downloaded files in the following cache directory so that they
# can be re-used without re-downloading.
cache=${cache:-$root/.cache}

# If the no_cache variable is not set, set it to an empty string to make sure
# the variable is bound.
no_cache=${no_cache:-}

# The directory to store SQL initialization scripts
sql_dir=/usr/local/lib/postgresql/initdb.d

. $root/include
